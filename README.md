# AlexLab Cloud

> Building, learning, and having fun 🏔️

<img src='https://gitlab.com/alexlab-cloud/design/kit/-/raw/main/packages/logos/assets/alexlab-cloud-rotating.gif' alt="AlexLab Cloud logo" align="right" height="200" />

[![AlexLab Cloud documentation][badges.docs]][links.alexlab-cloud-docs]


I'm an engineer living near Denver, Colorado, USA. **This is AlexLab Cloud, a
[dev garden][links.digital-gardens] for learning how to make robust and awesome software.** I use this
group to manage my personal projects with [namespacing](https://docs.gitlab.com/ee/user/namespace/) that expresses
information about each repo.

The bulk of the work here is my own, but I occasionally collaborate with
friends and my dad. I also store some projects on **[my personal profile][links.gl.alexcochran]** and
**[GitHub][links.gh.alexcochran]**.

[AlexLab Cloud][links.gl.alexlab-cloud] is enrolled in the [GitLab for Open Source Program][links.gl.open-source-prog].

---

<div id="badges" align="center">

[![acochran.dev][badges.personal-website]][links.personal-website]
[![GitLab: alexcochran][badges.gitlab-alexcochran]][links.gl.alexcochran]
[![GitHub: alexcochran][badges.github-alexcochran]][links.gh.alexcochran]

</div>

<!-- Links -->
[links.alexlab-cloud-docs]: https://docs.alexlab-cloud.dev
[links.personal-website]: https://acochran.dev
[links.digital-gardens]: https://maggieappleton.com/garden-history
[links.gl.alexcochran]: https://gitlab.com/alexcochran
[links.gh.alexcochran]: https://github.com/alexcochran
[links.gl.alexlab-cloud]: https://gitlab.com/alexlab-cloud
[links.gl.open-source-prog]: https://about.gitlab.com/solutions/open-source

<!-- Badges -->
[badges.docs]: https://img.shields.io/badge/Docs-441bda?style=for-the-badge
[badges.personal-website]: https://img.shields.io/badge/acochran.dev-blue?style=for-the-badge&color=%23470ff4
[badges.gitlab-alexcochran]: https://img.shields.io/badge/GitLab%3A%20alexcochran-%23554488?logo=gitlab&style=for-the-badge
[badges.github-alexcochran]: https://img.shields.io/badge/GitHub%3A%20alexcochran-%23000000?logo=github&style=for-the-badge
